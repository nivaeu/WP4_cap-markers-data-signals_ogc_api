from typing import Optional;
from fastapi import Depends , FastAPI, File, UploadFile , Form , Request;
from fastapi.staticfiles import StaticFiles;
from fastapi.responses import HTMLResponse;
from fastapi.security import HTTPBasic, HTTPBasicCredentials;
from typing import List;
import time;
import datetime;
import os;
import os.path;
from os import path
import aiohttp;
import aiofiles;
import asyncio;
import json as JSON;
import sys;
import random;
import hashlib;
import psycopg2;
from   classes.amGeoImporter  import amGeoImporter;
from   classes.amModel  import amModel;
from   classes.amTools  import amTool;
from   classes.amUser  import amUser;

security      = HTTPBasic();
amTool        = amTool();
amUser        = amUser();
cfg           = amTool.load( "./cfg/" , "config" );
myModel       = amModel();
geoImporter   = amGeoImporter( );
userParcels   = list();
endpoint_tags = [
    {
        "name"        : "layers",
        "description" : "Operations with Layers.",
    },
    {
        "name"        : "metadata",
        "description" : "Operations with Parcels Metadata.",
    },
    {
        "name"        : "calendar",
        "description" : "Operations with Parcels Calendar.",
    },
    {
        "name"        : "image",
        "description" : "Operations with Parcels Images.",
    },
    {
        "name"        : "aggregates",
        "description" : "Operations with Parcels Aggregates.",
    }
];
app = FastAPI( 
              title        = cfg["service"]["title"],
              description  = cfg["service"]["description"],
              version      = cfg["service"]["version"],
              openapi_tags = endpoint_tags , 
              redoc_url    = None , 
              docs_url     = "/" + cfg["service"]["docs_url"]
      );

app.mount( 
           "/"+cfg["uploads"]["images"]["service_name"] , 
           StaticFiles( directory = cfg["uploads"]["images"]["path"] ), 
           name=cfg["uploads"]["images"]["service_name"] 
         );

def getCredentials( credentials ):
    return { "username" : credentials.username, "password" : credentials.password }

async def isValidUser( credentials ):
    global userParcels;
    myOptions   = dict();
    userParcels = list();
    myOptions["un"] = credentials["username"];
    myOptions["pw"] = credentials["password"];

    hasUserAccess = await amUser.getUserAccess( myOptions );

    if( hasUserAccess is not False ):
        for parcelProperty in hasUserAccess[ "access" ] :
            userParcels.append( parcelProperty["parcel_id"] );
        return hasUserAccess;
    else:
        return False;

async def checkStatus( index ):
    if( index >= 20 ) : 
        print( "Reached n Tries. Exiting" );
        return False;

    taskResponse = await geoImporter.getResult();
    taskState    = taskResponse["task"]["state"];

    if( taskState != "COMPLETE" ):
        print( "Task still running" );
        index = index + 1;
        time.sleep( 3 );
        return await checkStatus( index );
    else:
        return True;

# Layers ( Create , Delete , Select )


@app.get("/") #Working
async def intro():
    amTool.logFile( "Server Started" );
    return { "res" : "Niva-ws base" };

@app.get("/layers_deprecated", tags=["layers"]) #Working
async def getLayers( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myResponse = await geoImporter.getLayers();
    return myResponse;

@app.get("/layers", tags=["layers"]) #Working
async def getLayers( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );
    myGeoObject     = { "layers" : { "layer" : [] } };

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myResponse = await myModel.getLayers();
    
    if( len( myResponse ) > 0 ):
        for item in myResponse:
            myGeoObject["layers"]["layer"].append( {"name" : item["table_name"] } );
        return myGeoObject;
    else:
        return [];

@app.post("/layers/{layer}", tags=["layers"]) #Working
async def createLayer( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None,
            filedata     : UploadFile = File(...) ):

    userCredentials = getCredentials( credentials );
    uploadsPath     = cfg["uploads"]["tmp"]["path"];
    extension       = os.path.splitext(filedata.filename)[1];
    newFilePath     = f"{uploadsPath}{layer}{extension}";
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( userAccess["isadmin"] < 0 ):
        return { "error" : "Only Admins have Access to this Resource" }
    else:
        with open( newFilePath , "wb+" ) as newFileHandler:
            newFileHandler.write( filedata.file.read() );

    layerExists = await geoImporter.layerExists( cfg["geoserver"]["workspace"] , "view_"+layer );
    if( layerExists ) : 
        return { "error" : "Layer ("+layer+") already Exists" };

    print();
    print( "** Creating GeoServer Task **" );
    await geoImporter.createTask( 
      { 
        "import" : { 
          "targetWorkspace" : { 
            "workspace" : { 
              "name":cfg["geoserver"]["workspace"]
            }
          },
          "targetStore" : { 
            "dataStore" : { 
              "name":cfg["geoserver"]["storename"]
            }
          }
        }
      }
    );

    print();
    print( "** Uploading File ["+ layer + extension +"] **" );
    await geoImporter.uploadFile( 
            filePath        = newFilePath , 
            fileName        = layer + extension , 
            fileContentType = filedata.content_type 
          );
    # return;

    print();
    print( "** Updating Layer Name to ["+ layer +"] **" );
    await geoImporter.addName( layer );

  # Last Parameter is the pk_sequence and can take the following values
  # assigned       : The value of the attribute in the newly inserted feature will be used (this assumes the “expose primary keys” flag has been enabled)
  # sequence       : The value of the attribute will be generated from the next value of a sequence indicated in the pk_sequence column.
  # autogenerated  : The column is an auto-increment one, the next value in the auto-increment will be used.
    print();
    print( "** Updating Our Foreign Key configruation **" );
    await myModel.addPKConfiguration( "public" , "view_"+layer , "fid" , "autogenerated" );

    # await geoImporter.addProjection( "EPSG:2100" );
    # await geoImporter.changeMode( "CREATE" );
    # await geoImporter.mapAttributes( {"type":"AttributesToPointGeometryTransform","latField":"Lat","lngField":"Lon"} );

    print();
    print( "** Executing GeoServer Task **" );
    await geoImporter.executeTask();
    taskResponse = await geoImporter.getResult();

    print();
    print( "** Response from GeoServer Task **" );
    print( taskResponse );
    taskCompleted = await checkStatus( 0 );
    if( taskCompleted == False ) :
        print( "Failed to Complete Task. Please check GeoServer." );
        return { "error" : "Failed to Complete Task. Please check GeoServer." };

    print();
    print( "** Creating View in Database ("+layer+") **" );
    await myModel.createView( layer );

    print();
    print( "** Deleting Layer on GeoServer ("+taskResponse["task"]["layer"]["name"]+") **" );
    deleteLayerResponse = await geoImporter.deleteLayer( cfg["geoserver"]["workspace"] , taskResponse["task"]["layer"]["name"] );
    print( deleteLayerResponse );

    print();
    print( "** Deleting Layer on GeoServer Recursive ("+taskResponse["task"]["layer"]["name"]+") **" );
    deleteLayerSimpleResponse = await geoImporter.deleteLayerSimple( cfg["geoserver"]["workspace"] , taskResponse["task"]["layer"]["name"] );
    print( deleteLayerSimpleResponse );

    print();
    print( "** Importing Layer from Database (" + "view_" + layer + " ) **" );
    myImportResult = await geoImporter.addLayer( 
      cfg["geoserver"]["workspace"] , 
      cfg["geoserver"]["storename"] , 
      "view_" + layer ,
      {
        "minx" : 25.2470000001234 , 
        "maxx" : 25.2498405498157 , 
        "miny" : 40.9758312141462 , 
        "maxy" : 40.977182003628
      }
      ,
      "EPSG:2100"
    );
    print( myImportResult );

    print();
    print( "** Deleting physical file from storage (" + newFilePath + " ) **" );
    os.remove( newFilePath );

    print();
    print( "** Creating Layer finished for Layer (" + "view_" + layer + " ) **" );

    return { "res": f"Layer '{layer}' uploaded at '{newFilePath}' and type '{filedata.content_type}' and extension '{extension}' "}

@app.delete("/layers/{layer}", tags=["layers"]) #Working
async def deleteLayer( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( userAccess["isadmin"] < 0 ):
        return { "error" : "Only Admins have Access to this Resource" }
    else:
        hasLayer = await geoImporter.layerExists( cfg["geoserver"]["workspace"] , "view_"+layer );
        if( hasLayer == True ):
            print();
            print( "** Deleting entries in Image for (" + layer + " ) **" );

            myLayer = await myModel.getLayer( layer );
            myImages = await myModel.getFeaturesWithImage( { "layer_name"  : layer } );

            for image in myImages :
                myImagesList = await myModel.getImage( {
                  "featureid"   : image["layer_id"] , 
                  "layer_name"  : layer 
                } );

                if( myImagesList is False ):
                    print ( "No Image available in : " + layer );
                else:
                # The Database Entry will be removed on "deleteEntity" further down
                # Now we only need to remove the Physical Files from our Storage
                    for image in myImagesList:
                        print( "264 : Sending '"+str( image["filename"] )+"' for Deletion." );
                        if( path.exists( cfg["uploads"]["images"]["path"] + image["filename"] ) ) :
                            os.remove( cfg["uploads"]["images"]["path"] + image["filename"] );

            # print();
            # print( "** Deleting entries in Calendar for (" + layer + " ) **" );
            # myDeleteResponse  = await myModel.deleteCalendar( {
                # "entity"            : "metadata" , 
                # "layername"         : layer 
            # });

            print();
            print( "** Deleting entries in MetaData for (" + layer + " ) **" );
            myDeleteResponse  = await myModel.deleteEntity( {
                "entity"            : "metadata" , 
                "layername"         : layer ,
                "return"            : "metadata_id"
            });

            print();
            print( "** Deleting View (" + "view_" + layer + " ) **" );
            await myModel.deleteView( "view_" + layer );

            print();
            print( "** Deleting Layer Table (" + layer + " ) **" );
            await myModel.deleteTable( layer );

            print();
            print( "** Removing PK Configuration for View (" + "view_" + layer + " ) **" );
            await myModel.removePKConfiguration( "view_" + layer );

            print();
            print( "** Deleting Layer on GeoServer (" + "view_" + layer + " ) **" );
            await geoImporter.deleteLayer( cfg["geoserver"]["workspace"] , "view_" + layer );

            print();
            print( "** Deleting Layer on GeoServer Datastore (" + "view_" + layer + " ) **" );
            await geoImporter.deleteLayerSimple( cfg["geoserver"]["workspace"] , "view_" + layer );

            print();
            print( "** Layer deletion complete (" + "view_" + layer + " ) **" );

            return { "res": f"Layer '{layer}' deleted" }
        else:
            return { "error" : "Layer '"+layer+"' does not exist in '"+cfg["geoserver"]["workspace"]+"' Workspace." };

@app.get("/layers/{layer}", tags=["layers"]) #Working
async def getLayerDetails( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    hasLayer = await geoImporter.layerExists( cfg["geoserver"]["workspace"] , "view_"+layer );
    if( hasLayer == True ):
        if( userAccess["isadmin"] < 0 ):
            myResponse = await myModel.getLayerData( "view_"+layer );
            myCurated  = list();
            for index , parcelData in enumerate( myResponse ) : 
                print( parcelData[ "Identifier" ] );
                if( parcelData[ "Identifier" ] in userParcels ):
                    myCurated.append( myResponse[ index ] );
            return myCurated;
        else:
            myResponse = await myModel.getLayerData( "view_"+layer );
            return myResponse;
    else:
       return { "error" : "Layer '"+layer+"' does not exist in '"+cfg["geoserver"]["workspace"]+"' Workspace." };

@app.get("/layers/{layer}/parcels/{parcel}", tags=["layers"]) #Working
async def getParcelDetails( 
            credentials  : HTTPBasicCredentials = Depends(security) , 
            layer        : Optional[str] = None , 
            parcel       : Optional[str] = None ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }
    else:
        hasLayer = await geoImporter.layerExists( cfg["geoserver"]["workspace"] , "view_"+layer );
        if( hasLayer == True ):
            myResponse = await myModel.getLayerData( "view_"+layer , parcel );
            return myResponse;
        else:
           return { "error" : "Layer '"+layer+"' does not exist in '"+cfg["geoserver"]["workspace"]+"' Workspace." };

# MetaData ( With JSON )

@app.put("/layers/{layer}/parcels/{parcel}/metadata_deprecated", tags=["metadata"]) #Working
async def addMetaData_old( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    myBody = await request.body();
    if ( amTool.isValidJSON( myBody ) == False ):
        return { "error" : "Invalid JSON" };

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ) : 
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            myMetaData = await myModel.getMetaData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if len( myMetaData ) <= 0 :
                myAddResponse  = await myModel.addEntity( {
                    "isUpdatable"       : True , 
                    "allowMultiInsert"  : False , 
                    "entity"            : "metadata" , 
                    "unique_id"         : "metadata_id" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "json"              : {} , 
                    "fields"            : [ 
                     "layer_id" , 
                     "layer_name" 
                    ]
                } );

                if( "error" in myAddResponse ):
                    print( str( myAddResponse["error"] ) );
                    return { "error" : "There was an error adding metadata. Check Server logs for details." };

            myAddResponse  = await myModel.addMetadata( {
                "isUpdatable"       : True , 
                "entity"            : "metadata" , 
                "layername"         : layer , 
                "featureid"         : myFeatureID , 
                "json"              : JSON.loads( myBody )
            });

            if( "error" in myAddResponse ):
                print( str( myAddResponse["error"] ) );
                return { "error" : "There was an error adding metadata. Check Server logs for details." };
            else:
                return { "fid" : myFeatureID };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.put("/layers/{layer}/parcels/{parcel}/metadata", tags=["metadata"]) #Working
async def addMetaData( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    myBody = await request.body();
    if ( amTool.isValidJSON( myBody ) == False ):
        return { "error" : "Invalid JSON" };

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ) : 
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID = myParcelData[0]["fid"];
            myMetaData  = await myModel.getMetaData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if len( myMetaData ) <= 0 :
                print( "Adding Metadata Entity" );
                myAddResponse  = await myModel.addEntity( {
                    "isUpdatable"       : True , 
                    "allowMultiInsert"  : False , 
                    "entity"            : "metadata" , 
                    "unique_id"         : "metadata_id" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "json"              : {} , 
                    "fields"            : [ 
                     "layer_id" , 
                     "layer_name" 
                    ]
                } );

                if( "error" in myAddResponse ):
                    print( str( myAddResponse["error"] ) );
                    return { "error" : "There was an error adding metadata. Check Server logs for details." };

            myAddResponse  = await myModel.addMetadata( {
                "isUpdatable"       : True , 
                "entity"            : "metadata" , 
                "layername"         : layer , 
                "featureid"         : myFeatureID , 
                "json"              : JSON.loads( myBody )
            });

            if( "error" in myAddResponse ):
                print( str( myAddResponse["error"] ) );
                return { "error" : "There was an error adding metadata. Check Server logs for details." };
            else:
                return { "fid" : myFeatureID };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.delete("/layers/{layer}/parcels/{parcel}/metadata", tags=["metadata"]) #Working
async def deleteMetaData( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myParcelData;
            myFeatureID    = myJSONResponse[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 
                myDeleteResponse  = await myModel.deleteMetadata( {
                    "entity"            : "metadata" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID
                });

                if( "error" in myDeleteResponse ):
                    print( str( myDeleteResponse["error"] ) );
                    return { "error" : "There was an error deleting Metadata. Check Server logs for details." };
                else:
                    return { "res" : "success" };
            else:
                return { 
                 "error" : "There is no entry for Metadata"
                };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.get("/layers/{layer}/parcels/{parcel}/metadata", tags=["metadata"]) #Working
async def getMetadata( 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error"  : "No Computed Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            myMetaData     = await myModel.getMetaData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if( "error" in myMetaData ):
                print( str( myMetaData["error"] ) );
                return { "error" : "There was an error retrieving data. Check Server logs for details." };
            else:
                if( len( myMetaData ) <= 0 ) : 
                    return { "error" : "No Metadata for ("+layer+")" };
                else:
                    if( len( response[0]["computed"] ) > 0 ):
                        return myMetaData[0]["computed"];
                    else:
                        return { "error" : "No Metadata for ("+layer+")" };
    else:
        return { 
          "error"  : "No Computed Data for ("+layer+")" 
        };

# Aggregates ( With JSON )

@app.put("/layers/{layer}/parcels/{parcel}/aggregates", tags=["aggregates"]) #Working
async def addAggregates( 
             request      : Request , 
             formdata     : Optional[str] = None,
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None,
            ):

    myBody = await request.body();
    if ( amTool.isValidJSON( myBody ) == False ):
        return { "error" : "Invalid JSON" };

    # myBody = formdata;
    # if ( amTool.isValidJSON( myBody ) == False ):
        # return { "error" : "Invalid JSON" };

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ) : 
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID = myParcelData[0]["fid"];
            myAggregates  = await myModel.getAggregatesData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if len( myAggregates ) <= 0 :
                print( "Adding Aggregates Entity" );
                myAddResponse  = await myModel.addEntity( {
                    "isUpdatable"       : True , 
                    "allowMultiInsert"  : False , 
                    "entity"            : "metadata" , 
                    "unique_id"         : "metadata_id" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "json"              : {} , 
                    "fields"            : [ 
                     "layer_id" , 
                     "layer_name" 
                    ]
                } );

                if( "error" in myAddResponse ):
                    print( str( myAddResponse["error"] ) );
                    return { "error" : "There was an error adding Aggregates. Check Server logs for details." };

            myAddResponse  = await myModel.addAggregates( {
                "isUpdatable"       : True , 
                "entity"            : "metadata" , 
                "layername"         : layer , 
                "featureid"         : myFeatureID , 
                "json"              : JSON.loads( myBody )
            });

            if( "error" in myAddResponse ):
                print( str( myAddResponse["error"] ) );
                return { "error" : "There was an error adding metadata. Check Server logs for details." };
            else:
                return { "fid" : myFeatureID };
    else:
        return {
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.delete("/layers/{layer}/parcels/{parcel}/aggregates", tags=["aggregates"]) #Working
async def deleteAggregates( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myParcelData;
            myFeatureID    = myJSONResponse[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 
                myDeleteResponse  = await myModel.deleteAggregates( {
                    "entity"            : "metadata" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID
                });

                if( "error" in myDeleteResponse ):
                    print( str( myDeleteResponse["error"] ) );
                    return { "error" : "There was an error deleting Aggregates. Check Server logs for details." };
                else:
                    return { "res" : "success" };
            else:
                return { 
                 "error" : "There is no entry for Aggregates"
                };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.get("/layers/{layer}/parcels/{parcel}/aggregates", tags=["aggregates"]) #Working
async def getAggregates( 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error"  : "No Computed Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            myAggregatesData = await myModel.getAggregatesData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if( "error" in myAggregatesData ):
                print( str( myAggregatesData["error"] ) );
                return { "error" : "There was an error retrieving data. Check Server logs for details." };
            else:
                if( len( myAggregatesData ) <= 0 ) : 
                    return { "error" : "No Aggregates for ("+layer+")" };
                else:
                    if( myAggregatesData[0]["aggregates"] is not None ):
                        if( len( myAggregatesData[0]["aggregates"] ) > 0 ):
                            return myAggregatesData[0]["aggregates"];
                        else:
                            return { "error" : "No Aggregates for ("+layer+")" };
                    else:
                        return { "error" : "No Aggregates for ("+layer+")" };
    else:
        return { 
          "error"  : "No Computed Data for ("+layer+")" 
        };

# MetaData ( Without JSON. Deprecated )

@app.put("/layers/{layer}/parcels/{parcel}/metadata_deprecated", tags=["metadata"]) #Working
async def addMetaData_deprecated( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    myBody = await request.body();
    if ( amTool.isValidJSON( myBody ) == False ):
        return { "error" : "Invalid JSON" };

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myLayerData = await myModel.getLayer( layer , parcel );

    if( "error" not in myLayerData ):
        if( len( myLayerData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myLayerData;
            myFeatureID    = myJSONResponse[0]["fid"];
            myAddResponse  = await myModel.addEntity( {
                "isUpdatable"       : True , 
                "allowMultiInsert"  : False , 
                "entity"            : "metadata" , 
                "unique_id"         : "metadata_id" , 
                "layername"         : layer , 
                "featureid"         : myFeatureID , 
                "json"              : JSON.loads( myBody ) , 
                "fields"            : [ 
                 "calculationdate",
                 "rulename",
                 "colorcode",
                 # "parcelcode",
                 "cultivationdeclaredcode",
                 "cultivationdeclaredname",
                 "cultivation1stpredictioncode",
                 "probability1stprediction",
                 "cultivation2ndpredictioncode",
                 "probability2stprediction",
                 "layer_id" , 
                 "layer_name" 
                ]
            });

            if( "error" in myAddResponse ):
                print( str( myAddResponse["error"] ) );
                return { "error" : "There was an error adding metadata. Check Server logs for details." };
            else:
                return { "success" : myAddResponse };
    else:
        return { 
          "error"  : str( myLayerData["error"] ) 
        };

@app.delete("/layers/{layer}/parcels/{parcel}/metadata_deprecated", tags=["metadata"]) #Working
async def deleteMetaData_deprecated(
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 
                myDeleteResponse  = await myModel.deleteEntity( {
                    "entity"            : "metadata" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "return"            : "metadata_id"
                });

                if( "error" in myDeleteResponse ):
                    print( str( myDeleteResponse["error"] ) );
                    return { "error" : "There was an error deleting metadata. Check Server logs for details." };
                else:
                    return { "res" : "Metadata entries deleted." };
            else:
                return { 
                 "error" : "There is no entry for metadata"
                };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

# Calendar ( Insert or Delete )

@app.put("/layers/{layer}/parcels/{parcel}/calendar", tags=["calendar"]) #Working
async def addCalendar( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    myBody = await request.body();
    if ( amTool.isValidJSON( myBody ) == False ):
        return { "error" : "Invalid JSON" };

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ) : 
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            myMetaData = await myModel.getMetaData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if len( myMetaData ) <= 0 :
                myAddResponse  = await myModel.addEntity( {
                    "isUpdatable"       : True , 
                    "allowMultiInsert"  : False , 
                    "entity"            : "metadata" , 
                    "unique_id"         : "metadata_id" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "json"              : {} , 
                    "fields"            : [ 
                     "layer_id" , 
                     "layer_name" 
                    ]
                });

                if( "error" in myAddResponse ):
                    print( str( myAddResponse["error"] ) );
                    return { "error" : "There was an error adding metadata. Check Server logs for details." };

            myAddResponse  = await myModel.addCalendar( {
                "isUpdatable"       : True , 
                "entity"            : "metadata" , 
                "layername"         : layer , 
                "featureid"         : myFeatureID , 
                "json"              : JSON.loads( myBody )
            });

            if( "error" in myAddResponse ):
                print( str( myAddResponse["error"] ) );
                return { "error" : "There was an error adding calendar. Check Server logs for details." };
            else:
                return { "fid" : myFeatureID };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.get("/layers/{layer}/parcels/{parcel}/calendar", tags=["calendar"]) #Working
async def getCalendar( 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error"  : "No Calendar Data for ("+layer+")" };
        else : 
            myFeatureID    = myParcelData[0]["fid"];
            myGetResponse  = await myModel.getCalendarData( {
              "layer_name" : layer , 
              "featureid"  : myFeatureID 
            });

            if( "error" in myGetResponse ):
                print( str( myGetResponse["error"] ) );
                return { "error" : "There was an error retrieving data. Check Server logs for details." };
            else:
                return myGetResponse;
    else:
        return { 
          "error"  : "No Calendar Data for ("+layer+")" 
        };

@app.delete("/layers/{layer}/parcels/{parcel}/calendar", tags=["calendar"]) #Working
async def deleteCalendar( 
             request      : Request , 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myParcelData;
            myFeatureID    = myJSONResponse[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 
                myDeleteResponse  = await myModel.deleteCalendar( {
                    "entity"            : "metadata" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID
                });

                if( "error" in myDeleteResponse ):
                    print( str( myDeleteResponse["error"] ) );
                    return { "error" : "There was an error deleting Calendar. Check Server logs for details." };
                else:
                    return { "res" : "success" };
            else:
                return { 
                 "error" : "There is no entry for Calendar"
                };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

# Image ( Insert or Delete )

@app.put("/layers/{layer}/parcels/{parcel}/images", tags=["image"]) #Working
async def addImage( 
             credentials            : HTTPBasicCredentials = Depends(security) , 
             layer                  : Optional[str] = None,
             parcel                 : Optional[str] = None
             , location_coordinates_x : float = Form(...) 
             , location_coordinates_y : float = Form(...) 
             , gyroscopic_x           : float = Form(...) 
             , gyroscopic_y           : float = Form(...) 
             , gyroscopic_z           : float = Form(...) 
             , description            : str = Form(...)  
             , Manufacturer           : str = Form(...)  
             , Platform               : str = Form(...)  
             , Version                : int = Form(...)  
             , Date                   : str = Form(...)  
             , filedata               : UploadFile = File(...) 
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myStringHash    = amTool.getUID( str( layer ) + str( filedata.filename ) );
    uploadsPath     = cfg["uploads"]["images"]["path"];
    extension       = os.path.splitext( filedata.filename )[1];
    imageName       = str(layer) + "_" + hashlib.md5( myStringHash.encode() ).hexdigest()+extension;
    newFilePath     = f"{uploadsPath}{imageName}";

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID = myParcelData[0]["fid"];
            myMetaData = await myModel.getMetaData( {
                "layer_name" : layer , 
                "featureid"  : myFeatureID 
            } );

            if len( myMetaData ) <= 0 :
                myAddResponse  = await myModel.addEntity( {
                    "isUpdatable"       : True , 
                    "allowMultiInsert"  : False , 
                    "entity"            : "metadata" , 
                    "unique_id"         : "metadata_id" , 
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "json"              : {} , 
                    "fields"            : [ 
                     "layer_id" , 
                     "layer_name" 
                    ]
                });

                if( "error" in myAddResponse ):
                    print( str( myAddResponse["error"] ) );
                    return { "error" : "There was an error adding metadata. Check Server logs for details." };

            with open( newFilePath , "wb+" ) as newFileHandler:
                newFileHandler.write( filedata.file.read() );

            response = await myModel.addImage( {
              "layerId"                : myFeatureID , 
              "layerName"              : layer ,
              "filename"               : imageName , 
              "form"                   : {
               "filename"               : imageName , 
               "location_coordinates_x" : location_coordinates_x,
               "location_coordinates_y" : location_coordinates_y,
               "gyroscopic_x"           : gyroscopic_x , 
               "gyroscopic_y"           : gyroscopic_y , 
               "gyroscopic_z"           : gyroscopic_z,
               "description"            : description,
               "Manufacturer"           : Manufacturer,
               "Platform"               : Platform,
               "Version"                : Version,
               "Date"                   : Date
              }
            } );

            if( "error" in response ):
                print( str( response["error"] ) );
                return { "error" : "There was an error adding image. Check Server logs for details." };
            else:
                return { "fid" : myFeatureID , "image" : newFilePath };
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };

@app.delete("/layers/{layer}/parcels/{parcel}/images", tags=["image"]) #Working
async def deleteImages( 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myParcelData;
            myFeatureID    = myJSONResponse[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 

                response = await myModel.getImage( {
                  "featureid"   : myFeatureID , 
                  "layer_name"  : layer 
                } );
                
                if( response is False ):
                    return { "error" : "No Image available" };
                else:
                    myImagesObject = response;

                    for image in myImagesObject:
                        myImageName = image["filename"];
                        print( "662 : Sending '"+str( myImageName )+"' for Deletion." );

                        myDeleteResponse  = await myModel.deleteImage( {
                            "layername"         : layer , 
                            "featureid"         : myFeatureID , 
                            "imageName"         : myImageName
                        });

                        if( path.exists( cfg["uploads"]["images"]["path"] + myImageName ) ) :
                            os.remove( cfg["uploads"]["images"]["path"] + myImageName );
            else:
                return { "error" : "There is no entry for Image" };
    else:
        return { "error"  : "No Layer Data for ("+layer+")" };

@app.delete("/layers/{layer}/parcels/{parcel}/images/{image_name}", tags=["image"]) #Working
async def deleteImage( 
             credentials  : HTTPBasicCredentials = Depends(security) , 
             layer        : Optional[str] = None,
             parcel       : Optional[str] = None,
             image_name   : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myJSONResponse = myParcelData;
            myFeatureID    = myJSONResponse[0]["fid"];
            hasEntity      = await myModel.hasEntity( "metadata" , myFeatureID );

            if( hasEntity == True ) : 
                print( image_name );

                myDeleteResponse  = await myModel.deleteImage( {
                    "layername"         : layer , 
                    "featureid"         : myFeatureID , 
                    "imageName"         : image_name
                });

                if( "error" in myDeleteResponse ):
                    return myDeleteResponse;
                else:
                    if( path.exists( cfg["uploads"]["images"]["path"] + image_name ) ) :
                        os.remove( cfg["uploads"]["images"]["path"] + image_name );
                    return { "success" : "Image : '"+image_name+"' deleted." };
            else:
                return { "error" : "There is no entry for Image" };
    else:
        return { "error"  : "No Layer Data for ("+layer+")" };

@app.get("/layers/{layer}/parcels/{parcel}/images", tags=["image"]) #Working
async def getImage( 
             credentials            : HTTPBasicCredentials = Depends(security) , 
             layer                  : Optional[str] = None,
             parcel                 : Optional[str] = None
            ):

    userCredentials = getCredentials( credentials );
    userAccess      = await isValidUser( userCredentials );

    if( userAccess == False ):
        return { "error" : "Invalid User" };

    if( amUser.hasAccessToParcel( parcel ) == False ):
        return { "error" : "No Access to Resource" }

    myParcelData = await myModel.getLayer( layer , parcel );

    if( "error" not in myParcelData ):
        if( len( myParcelData ) <= 0 ) : 
            return { "error" : "No Layer Data for ("+layer+")" };
        else : 
            myFeatureID = myParcelData[0]["fid"];

            response = await myModel.getImage( {
              "featureid"   : myFeatureID , 
              "layer_name"  : layer 
            } );

            if( "error" in response ):
                print( str( response["error"] ) );
                return { "error" : "There was an error getting image. Check Server logs for details." };
            else:
                return response;
    else:
        return { 
          "error"  : "No Layer Data for ("+layer+")" 
        };